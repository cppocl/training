/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain alloc copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cassert>
#include <memory>

int main()
{
    /* C++ allocators are used as a means to provide custom allocation instead of new and delete.
       The standard library classes such as string, list, vector, etc. use default allocators.
       Placement new or the construct function is used for initialisation of objects. */

    // Use the allocator class to allocate memory for 10 uninitialised int objects.
    std::allocator<int> alloc;
    int* pi = alloc.allocate(10);

    // Initialise the first two int objects with a value.
    // Placement new syntax is used to initialise the pointers.
    int* p1 = new (pi) int(1);
    int* p2 = new (pi + 1) int(2);
    assert(*p1 == 1);
    assert(*p2 == 2);
    alloc.deallocate(pi, 10);

    // Allocate and initialise the int with a value.
    int* p3 = alloc.allocate(1);
    new (p3) int(3);
    assert(p1 != p3);
    assert(*p3 == 3);
    alloc.deallocate(p3, 1);

    // Using allocator_traits, allocate, construct and deallocate the int object.
    int* p4 = std::allocator_traits<std::allocator<int>>::allocate(alloc, 1);
    std::allocator_traits<std::allocator<int>>::construct(alloc, p4, 4);
    assert(*p4 == 4);
    std::allocator_traits<std::allocator<int>>::deallocate(alloc, p4, 1);
}

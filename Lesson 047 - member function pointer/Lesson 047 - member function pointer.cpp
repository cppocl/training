/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>

class Number
{
public:
    Number(int number) noexcept
        : m_number(number)
    {
    }

    void SetNumber(int number) noexcept
    {
        m_number = number;
    }

    bool Print()
    {
        std::cout << m_number << std::endl;
        return m_number > 100;
    }

private:
    int m_number;
};

int main()
{
    // Define a pointer to member function variable called print_number_ptr and assign it.
    // Because a class can have member variables, and member functions can access these member variables,
    // It's important to note the class type is required as part of the pointer definition.
    Number number(99);
    bool (Number::*print_number_ptr)() = &Number::Print;

    // Use the pointer to function variable to call print_number function.
    // The operator .* operator is used to access member function pointer of an object.
    // If number was a pointer, i.e Number* number, then ->* operator would be required.
    bool is_large = (number.*print_number_ptr)();
    std::string message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    number.SetNumber(101);
    is_large = (number.*print_number_ptr)();
    message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    // Perform the same as above, but using a Number pointer and operator ->*
    Number* number_ptr = &number;
    number_ptr->SetNumber(98);
    is_large = (number_ptr->*print_number_ptr)();
    message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    number_ptr->SetNumber(102);
    is_large = (number_ptr->*print_number_ptr)();
    message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    return 0;
}

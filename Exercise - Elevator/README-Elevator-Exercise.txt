Challenge:

Create a program that uses an onject-oriented approach to solve the following conditions:

1. Support a single elevator.
2. Moves between 3 floors (1 to 3).
3. Call the lift from any floor.
4. Select any floor inside the elevator.
5. Show the current floor of the elevator at every level, including direction of travel, up, down or not moving.
6. Support an emergency stop with alarm.
7. Support a wait feature that causes the elevator state to change state and report current action or print No Action.


Hints:

The wait feature might report moving to floor 2, or whatever messages are deemed important.

/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>
#include <string>

class Main
{
public:
    // Set the floor that's wanted from within the elevator.
    void SelectFloor(int floor_number)
    {
        // TODO: place your code here.
        std::cout << "floor not implemented" << std::endl;
    }

    // Call the elevator from outside on the landing or lobby.
    void CallElevator(int current_floor)
    {
        // TODO: place your code here.
        std::cout << "call not implemented" << std::endl;
    }

    void Show()
    {
        // TODO: place your code here.
        std::cout << "show not implemented" << std::endl;
    }

    void Alarm()
    {
        // TODO: place your code here.
        std::cout << "alarm not implemented" << std::endl;
    }

    void Wait()
    {
        // TODO: place your code here.
        std::cout << "wait not implemented" << std::endl;
    }

private:
    // Put any of your data here.
};

std::string UserInput()
{
    char input[1000];
    std::string user_input;
    std::cin.getline(input, sizeof(input));
    user_input = input;
    return user_input;
}

int main()
{
    Main my_main;

    std::string user_input = UserInput();

    while (user_input != "exit")
    {
        std::string command;
        int argument = -1;

        std::size_t pos = user_input.find(' ');
        if (pos != std::string::npos)
        {
            command = user_input.substr(0, pos);
            argument = std::stoi(user_input.substr(pos + 1));
        }
        else
            command = user_input;

        if (command == "help")
        {
            std::string messages[]
            {
                "exit                 ... Exit the emulator",
                "floor <floor number> ... Select wanted floor from within the elevator",
                "call <current floor> ... Call the elevator to the current floor from a landing",
                "show                 ... Show the current elevator floor and direction of travel of up,",
                "                         down, or nothing when not moving)",
                "alarm                ... Stop the elevator and sound the alarm when trapped inside the elevator",
                "wait <current floor> ... Wait at the landing for the elevator to arrive,"
                "                         while show updates the elevator progress."
            };

            for (std::string const& message : messages)
                std::cout << message << std::endl;
        }
        else if (command == "floor")
            my_main.SelectFloor(argument);
        else if (command == "call")
            my_main.CallElevator(argument);
        else if (command == "show")
            my_main.Show();
        else if (command == "alarm")
            my_main.Alarm();
        else if (command == "wait")
            my_main.Wait();
        else if (!command.empty())
            std::cout << "Unrecognised or invalid use of command" << std::endl;

        user_input = UserInput();
    }
}
